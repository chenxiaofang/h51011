//获取当前时间
var now = new Date();

//从时间中获取时，分，秒。
var h = now.getHours();
var m = now.getMinutes();
var s = now.getSeconds();

//将小时数转为12小时制。
h = h%12;

//计算出现在距离上一个12点过了多少秒。
var du = h*3600+m*60+s;

//获得三个针的div元素
var hp = document.getElementById("hp");
var mp = document.getElementById("mp");
var sp = document.getElementById("sp");

//设置三个元素动画延时时间。
hp.style.animationDelay = -du+"s";
mp.style.animationDelay = -du+"s";
sp.style.animationDelay = -du+"s";