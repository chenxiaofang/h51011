
setInterval(function(){
    showMouse();
    showMouse();
    showMouse();
},2000);


//随机出现一只老鼠
function showMouse(){
    //随机生成一个0-15的随机整数
    var ind = Math.random()*16;
    ind = Math.floor(ind);

    //让对应编号的老鼠出现
    $("img").eq(ind).addClass("show");
    $("img")[ind].nothit = true;

    //1.5秒后，老鼠消失。
    setTimeout(() => {
        $("img").eq(ind).removeClass("show");
    }, 3000);
}


//给老鼠绑定鼠标点击事件

$("img").on("click",function(){
    $(this).removeClass("show");

    $("#hit")[0].load();
    //播放音效
    $("#hit")[0].play();

    //判断是否已经点过这只老鼠
    if(this.nothit){
        //加分
        scoreIncrease();
        this.nothit = false;
    }

    
});


var s = 0;
function scoreIncrease(){
    s = s+10;
    $("#score").text("得分："+s);
}


//拖拽内容时不在新的页面打开
document.body.ondrop = function(e){
    e.stopPropagation();
    e.preventDefault();
}
document.body.ondragend = function(e){
    e.preventDefault();
}