
//找到所有琴键的div
var allKeys = document.getElementsByClassName("key");

//找到所有的audio标签元素
var allSound = document.getElementsByClassName("sound");


//为每一个键div添加鼠标事件监听
for (var i = 0;i<allKeys.length;i++){
    allKeys[i].ind = i;
    
    //监听鼠标指针进入事件
    allKeys[i].onmouseenter = function(e){
        pressKey(e.target.ind);
    }

    //监听鼠标指针离开事件
    allKeys[i].onmouseleave = function(e){
        upKey(e.target.ind);
    }

}

//按下某个琴键
function pressKey(index){
    allSound[index].load();
    allSound[index].play();

    allKeys[index].style.transition = "none";
    allKeys[index].classList.add("press-down");
}

//弹起某个琴键
function upKey(index){
    allKeys[index].style.transition = "all 1s";
    allKeys[index].classList.remove("press-down");
}


//键盘按下事件
document.body.onkeydown = function(e){
    var k = e.key*1;
    if(allKeys[k-1].isDown){
        return;
    }
    if(1<=k&&k<=9){
        pressKey(k-1);
        allKeys[k-1].isDown = true;
    }
}

//键盘弹起事件。
document.body.onkeyup = function(e){
    var k = e.key*1;
    if(1<=k&&k<=9){
        upKey(k-1);
        allKeys[k-1].isDown = false;
    }
}

