
    //console.log(111);
    //1.获取左右按钮
    var left = this.document.querySelector('.shuffling_left');
    var right = this.document.querySelector('.shuffling_right');
    var ul = document.querySelector("ul");
    var focus = document.querySelectorAll("li");

    // 5.当前显示li的索引值
    var i = 0;
    // 3.给侧按钮添加点击事件
    right.addEventListener('click', function () {
        // 6.先更改要显示下一张图的索引值,根据索引值实现下一张图片的显示
        i++;
        console.log(i); // 1 => 340; 2 => 680; 3 =? 1020

        // 7.计算每次点击时要移动的宽度值
        var w = i * 340;

        // 8.判断在 显示是第7张图片 的时候,直接跳转至第一张图
        if(i >= 6){
            i = 0;
            
            // 9.等上次过度动画执行完毕后重新设置动画时间 原动画时间300
            setTimeout(function(){
                ul.style.transitionDuration = "0s";
                var w = i * 340;
                ul.style.left = `-${w}px`;
                // 10.确保上面三行代码执行完毕后重新定义过度动画时间
                setTimeout(function(){
                    ul.style.transitionDuration = "0.3s";
                },20)
 
            },300)

        }
        
        // 有9个li
        // 向左边移动一个li加间隙的距离 显示下一张li 需要知道当前显示li的索引值
        // 默认值0
        // 4.
        ul.style.left = `-${w}px`;
    })
    // 3.给左侧按钮添加点击事件
    left.addEventListener('click', function () {
        console.log(i);
        if(i === 0){
            i = 6;
            ul.style.transitionDuration = "0s";
            var w = i * 340;
            ul.style.left = `-${w}px`;
            setTimeout(function(){
                ul.style.transitionDuration = "0.3s";
                i--;
                var w = i * 340;
                ul.style.left = `-${w}px`;
            },20)
        }else{
            i--;
            var w = i * 340;
            ul.style.left = `-${w}px`;
        }

        
    })
