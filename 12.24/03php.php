<?php

    // $_GET;
    // print_r($_GET["callback"]);
    // 
    
    // jsonp方法跨域是php的代码
    $call = $_GET["callback"];
    $arr = array("name"=>"张三");
    $data = json_encode($arr);
    print_r("${call}($data)");


    // 普通的请求跨域的代码
    // header("Access-Control-Allow-Origin","*");
    // $arr = array("name"=>"张三");
    // $data = json_encode($arr);
    // print_r($data);


?>