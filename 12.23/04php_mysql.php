<?php
    header("Content-type:text/html;charset=utf-8");
    // 连接数据库
    // new mysqli(IP地址,数据库的用户名,数据库的密码,数据库名称);
    $mysqli = new mysqli("localhost","root","root","db_yuedu");

    // php中对象调用属性使用: 对象名称->属性名; 获取
    // $mysqli->connect_errno 判断数据库是否连接成功; 0表示成功 没有值表示失败
    // echo $mysqli->connect_errno;
    if($mysqli->connect_errno != 0){
        // 结束代码运行(如果连接数据库失败,直接停止运行代码)
        die();
    }

    // 在数据库中查询数据 先声明sql语句,字符串格式
    $sql = "select * from t_articles limit 10";
    // 使用$mysqli对象下的query()方法执行sql语句
    // 参数是sql语句
    // sql语句是查询语句时返回值是一个对象
    $res = $mysqli->query($sql);
    // 
    // print_r($res);
    // 获取对象中的数据
    // fetch_assoc()每调用一次就会获取一条数据
    // $data = $res->fetch_assoc();
    // $data1 = $res->fetch_assoc();
    // $data2 = $res->fetch_assoc();
    // print_r($data);// 关联数组
    // print_r($data1);
    // print_r($data2);

    // $arr = array('a',"b","c");
    // 往数组内追加一个值
    // $arr[] = "d";
    // print_r($arr);

    // $data = array();
    for($i = 0;$i < $res->num_rows;$i++){
        $data[] = $res->fetch_assoc();
    }
    print_r(json_encode($data));

?>