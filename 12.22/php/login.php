<?php
    header("Content-type:text/html;charset=utf-8");

    // 1.自定义一个用户,用来判断登录
    // $arr = array('user'=>'aaa','pass'=>'123123');
    $user = "aaa";
    $pass = "123123";

    // 判断客户端发送过来的数据是否与自定义数据一致
    // 2.查看是否获取成功客服端传递过来的值
    // print_r($_POST);
    $username = $_POST['username'];
    $password = $_POST['password'];

    // 3.先判断用户是否存在
    if($username !== $user){
        $arr = array("success"=>0,"error"=>"当前用户不存在");
    }else if($password !== $pass){
        $arr = array("success"=>0,"error"=>"密码错误");
    }else{
        $arr = array("success"=>1,"data"=>"登录成功");
    }

    print_r(json_encode($arr));

?>