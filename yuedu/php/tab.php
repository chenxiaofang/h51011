<?php
    // 这个页面的请求是: 获取全部分类
    // 1.从数据库获取, 连接数据库
    $mysqli = new mysqli("localhost","root","root","db_yuedu");
    // 2.判断是否连接成功,失败直接结束代码
    if($mysqli->connect_errno != 0){
        die();
    }

    // 3.查询表格的sql语句
    $sql = "select * from t_types";
    // 4.执行sql语句
    $res = $mysqli->query($sql);
    // 5.得到对象内的具体内容
    for($i = 0;$i < $res->num_rows;$i++){
        $data[] = $res->fetch_assoc();
    }
    //6.输出,也就是返回响应数据
    print_r(json_encode($data));

?>