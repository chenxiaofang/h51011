<?php
// 单行注释
// 在文件最上方设置编码格式
header('Content-type:text/html;charset=utf-8');
/*
    多行注释
    语法: <?php 运行代码 ?>
    html文件内不能写php代码
    php文件内可以编辑html代码
    PHP文件中每个语句都要以;分号结束(最后一句可以不用)
*/

// 将内容在页面输出
    echo (123);
    echo "文字";

    /*
    声明变量
    $自定义名称 = 值;
    在php中拼接使用.点代替了js里的+
    使用双引号, 在双引号内${n}或{$n}来拼接
    */ 
    $str = "这是一个字符串";
    echo $str."string";

    $str1 = "aaa${str}aaa";
    $str2 = "aaa{$str}aaa";
    echo $str1;
    echo $str2;


    $num = 12;
    echo $num + 12;
    echo "---";
    echo $num - 12;
    echo "---";
    echo $num * 12;
    echo "---";
    echo $num / 12;
    echo "---";
    echo $num % 12;


    $bool = true;
    // echo $bool; //1

    // 判断语句与js相同
    if($bool){
        echo "true";
    }else{
        echo "false";
    }
    // 循环语句与js一致
    for($i = 0;$i < 10;$i++){
        echo $i;
    }

    // 申明一个索引数组 print_r(数组对象)
    $arr = array("a","b","c");
    echo $arr[1];
    // Array ( [0] => a [1] => b [2] => c )
    print_r($arr);
    // 输出数组的长度
    echo count($arr);

    foreach($arr as $item){
        echo $item;
    }
    // 关联数组
    $obj = array("name"=>"张三","age"=>18);
    print_r($obj);
    echo $obj["name"];
    foreach($obj as $key => $item){
        echo $key.$item;
    }

?>